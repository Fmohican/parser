/* Info Laurena: 
 PeteOPeng Level: 88 - 99 
 Player :  5 - 15 :
*/
DECLARE @BoxId SMALLINT = 5989
DECLARE @BoxDesign SMALLINT = 0


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES
	(@BoxDesign, @BoxId, '0', '0', '4489', '0', '1', '0', '10'), /* Renegade */
	(@BoxDesign, @BoxId, '0', '0', '4488', '0', '1', '0', '10'), /* Avenging Angel */
	(@BoxDesign, @BoxId, '0', '0', '4487', '0', '1', '0', '10'), /* Archmage */
	(@BoxDesign, @BoxId, '0', '0', '4813', '0', '1', '0', '5'), /* Laurena's */
	(@BoxDesign, @BoxId, '0', '0', '4812', '0', '1', '0', '5'), /* Lucifer's */
	(@BoxDesign, @BoxId, '0', '0', '8368', '0', '1', '0', '1'), /* Laurena's Witch Hat */
	(@BoxDesign, @BoxId, '0', '0', '2514', '0', '1', '0', '20'), /* Small Ruby of Completion */
	(@BoxDesign, @BoxId, '0', '0', '2515', '0', '1', '0', '20'), /* Small Sapphire of Completion */
	(@BoxDesign, @BoxId, '0', '0', '2516', '0', '1', '0', '20'), /* Small Obisidian of Completion */
	(@BoxDesign, @BoxId, '0', '0', '2517', '0', '1', '0', '20'), /* Small Topaz of Completion */
	(@BoxDesign, @BoxId, '0', '0', '2518', '0', '1', '0', '15'), /* Ruby of Completion*/
	(@BoxDesign, @BoxId, '0', '0', '2519', '0', '1', '0', '15'), /* Sapphire of Completion*/
	(@BoxDesign, @BoxId, '0', '0', '2520', '0', '1', '0', '15'), /* Obsidian of Completion*/
	(@BoxDesign, @BoxId, '0', '0', '2521', '0', '1', '0', '15'), /* Topaz of Completion*/
	(@BoxDesign, @BoxId, '0', '0', '4835', '0', '1', '0', '25'), /* Laurean's Necklace */
	(@BoxDesign, @BoxId, '0', '0', '4836', '0', '1', '0', '25'), /* Laurean's Ring */
	(@BoxDesign, @BoxId, '0', '0', '4837', '0', '1', '0', '25'), /* Laurean's Bracelet */
	(@BoxDesign, @BoxId, '0', '0', '9375', '0', '1', '0', '1'), /* Little Witch */
	(@BoxDesign, @BoxId, '0', '0', '2434', '0', '1', '0', '7'), /* Twilight Essence */
	(@BoxDesign, @BoxId, '0', '0', '2285', '0', '2', '0', '60'), /* Shinig Blue Soul */
	(@BoxDesign, @BoxId, '0', '0', '2282', '0', '5', '0', '60') /* Angel's Feather */

