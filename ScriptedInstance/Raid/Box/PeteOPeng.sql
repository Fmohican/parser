/* Info :
 PeteOPeng Level: 20 - 99 
 Player :  5 - 15 :
 Delay : 60 min. 
 Reputation : +100 
 Gold : 1000 - 2000
 Data : 
 Damage% : yes
 TeleportAll : yes
 OneSeal : no
 */
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 12


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES

	(@BoxDesign, @BoxId, '0', '7', '5141', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5144', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5145', '0', '5', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '5146', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4166', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4165', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4215', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4189', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4190', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4191', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4192', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4099', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4201', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4204', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4205', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4208', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '9368', '0', '1', '0', '1')