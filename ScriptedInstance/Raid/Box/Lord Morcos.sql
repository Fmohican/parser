/* Info Lord Morcos: 
 Level: 30 - 99 
*/
DECLARE @BoxId SMALLINT = 882
DECLARE @BoxDesign SMALLINT = 99


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES
	(@BoxDesign, @BoxId, '1', '7', '567', '0', '1', '0', '5'), /* Shell Full W */
	(@BoxDesign, @BoxId, '1', '7', '570', '0', '1', '0', '5'), /* Shell Special W*/
	(@BoxDesign, @BoxId, '1', '7', '573', '0', '1', '0', '5'), /* Shell PvP W*/
	(@BoxDesign, @BoxId, '1', '7', '576', '0', '1', '0', '5'), /* Shell Perfect W*/
	(@BoxDesign, @BoxId, '1', '7', '579', '0', '1', '0', '5'), /* Shell Full A*/
	(@BoxDesign, @BoxId, '1', '7', '582', '0', '1', '0', '5'), /* Shell Special A*/
	(@BoxDesign, @BoxId, '1', '7', '585', '0', '1', '0', '5'), /* Shell PvP A*/
	(@BoxDesign, @BoxId, '1', '7', '588', '0', '1', '0', '5'), /* Shell Perfect A*/
	(@BoxDesign, @BoxId, '0', '7', '350', '0', '1', '0', '25'), /* Weapon Sword */
	(@BoxDesign, @BoxId, '0', '7', '354', '0', '1', '0', '25'), /* Weapon Archer */
	(@BoxDesign, @BoxId, '0', '7', '357', '0', '1', '0', '25'), /* Weapon Mage */
	(@BoxDesign, @BoxId, '0', '7', '264', '0', '1', '0', '35'), /* Weapon Sword */
	(@BoxDesign, @BoxId, '0', '7', '267', '0', '1', '0', '35'), /* Weapon Archer */
	(@BoxDesign, @BoxId, '0', '7', '270', '0', '1', '0', '35'), /* Weapon Mage */
	(@BoxDesign, @BoxId, '0', '7', '4984', '0', '1', '0', '2'), /* C48 Swordman Armor */
	(@BoxDesign, @BoxId, '0', '0', '2514', '0', '1', '0', '20'), /* Small Ruby of Completion */
	(@BoxDesign, @BoxId, '0', '0', '1429', '0', '5', '0', '40'), /* Rainbow Pearl */
	(@BoxDesign, @BoxId, '0', '0', '2436', '0', '5', '0', '3'), /* Sellaim's Essence */
	(@BoxDesign, @BoxId, '0', '0', '1430', '0', '5', '0', '30'), /* Magic Eraser */
	(@BoxDesign, @BoxId, '0', '0', '9384', '0', '5', '0', '1'); /* Forebreather */
