/* Info Baron Berios: 
 Level: 30 - 99 
*/
DECLARE @BoxId SMALLINT = 999
DECLARE @BoxDesign SMALLINT = 99


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES
	(@BoxDesign, @BoxId, '1', '7', '567', '0', '1', '0', '5'), /* Shell Full W */
	(@BoxDesign, @BoxId, '1', '7', '570', '0', '1', '0', '5'), /* Shell Special W*/
	(@BoxDesign, @BoxId, '1', '7', '573', '0', '1', '0', '5'), /* Shell PvP W*/
	(@BoxDesign, @BoxId, '1', '7', '576', '0', '1', '0', '5'), /* Shell Perfect W*/
	(@BoxDesign, @BoxId, '1', '7', '579', '0', '1', '0', '5'), /* Shell Full A*/
	(@BoxDesign, @BoxId, '1', '7', '582', '0', '1', '0', '5'), /* Shell Special A*/
	(@BoxDesign, @BoxId, '1', '7', '585', '0', '1', '0', '5'), /* Shell PvP A*/
	(@BoxDesign, @BoxId, '1', '7', '588', '0', '1', '0', '5'), /* Shell Perfect A*/
	(@BoxDesign, @BoxId, '0', '7', '993', '0', '1', '0', '25'), /* Armor Sword */
	(@BoxDesign, @BoxId, '0', '7', '994', '0', '1', '0', '25'), /* Armor Archer */
	(@BoxDesign, @BoxId, '0', '7', '989', '0', '1', '0', '25'), /* Armor Mage */
	(@BoxDesign, @BoxId, '0', '0', '2395', '0', '3', '0', '30'), /* Blue Gem */
	(@BoxDesign, @BoxId, '0', '0', '2396', '0', '5', '0', '30'), /* Green Gem */
	(@BoxDesign, @BoxId, '0', '0', '2397', '0', '10', '0', '30'), /* Yellow Gem */
	(@BoxDesign, @BoxId, '0', '7', '4986', '0', '1', '0', '2'), /* C45 Swordman XBow */
	(@BoxDesign, @BoxId, '0', '0', '2517', '0', '1', '0', '20'), /* Small Topaz of Completion */
	(@BoxDesign, @BoxId, '0', '0', '1429', '0', '5', '0', '40'), /* Rainbow Pearl */
	(@BoxDesign, @BoxId, '0', '0', '2438', '0', '5', '0', '3'), /* Eperial's Essence */
	(@BoxDesign, @BoxId, '0', '0', '1430', '0', '5', '0', '30'), /* Magic Eraser */
	(@BoxDesign, @BoxId, '0', '0', '9387', '0', '5', '0', '1'); /* Lightbringer */
