/* Info Erenia: 
 PeteOPeng Level: 88 - 99 
 Player :  5 - 15 :
*/
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 24


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES
	(@BoxDesign, @BoxId, '0', '8', '4952', '0', '1', '0', '10'), /* Hellord Heavy Armour */
	(@BoxDesign, @BoxId, '0', '8', '4953', '0', '1', '0', '10'), /* Hellord Robe */
	(@BoxDesign, @BoxId, '0', '8', '4954', '0', '1', '0', '10'), /* Hellord Leather Armour */
	(@BoxDesign, @BoxId, '0', '8', '4964', '0', '1', '0', '5'), /* Hellord Sword */
	(@BoxDesign, @BoxId, '0', '8', '4965', '0', '1', '0', '5'), /* Hellord Wand */
	(@BoxDesign, @BoxId, '0', '8', '4966', '0', '1', '0', '5'), /* Hellord Bow */
	(@BoxDesign, @BoxId, '0', '8', '4961', '0', '1', '0', '10'), /* Hellord Crossbow */
	(@BoxDesign, @BoxId, '0', '8', '4962', '0', '1', '0', '10'), /* Hellord Spellgun */
	(@BoxDesign, @BoxId, '0', '8', '4963', '0', '1', '0', '10'), /* Hellord Dagger */
	(@BoxDesign, @BoxId, '0', '0', '4972', '0', '1', '0', '20'), /* Hellord Leather Hat */
	(@BoxDesign, @BoxId, '0', '0', '4945', '0', '1', '0', '20'), /* Hellord Helmet */
	(@BoxDesign, @BoxId, '0', '0', '4974', '0', '1', '0', '20'), /* Hellord  Headband*/
	(@BoxDesign, @BoxId, '0', '0', '4942', '0', '1', '0', '5'), /* Hellord Neckle */
	(@BoxDesign, @BoxId, '0', '0', '4943', '0', '1', '0', '5'), /* Hellord Ring */
	(@BoxDesign, @BoxId, '0', '0', '4944', '0', '1', '0', '5'), /* Hellord Bracel */
	(@BoxDesign, @BoxId, '0', '0', '4970', '0', '1', '0', '5'), /* Hellord Shoes */
	(@BoxDesign, @BoxId, '0', '0', '4969', '0', '1', '0', '5'), /* Hellord Gunlet */
	(@BoxDesign, @BoxId, '0', '0', '4975', '0', '1', '0', '1'), /* Erenia Crafted Horn */
	(@BoxDesign, @BoxId, '0', '0', '1686', '0', '1', '0', '10'), /* Devil Wings */
	(@BoxDesign, @BoxId, '0', '0', '5432', '0', '1', '0', '2'), /* ArchDaemon Wings */
	(@BoxDesign, @BoxId, '0', '0', '2516', '0', '1', '0', '35'), /* Small obisidan */
	(@BoxDesign, @BoxId, '0', '0', '2520', '0', '1', '0', '30'), /* obsidian */
	(@BoxDesign, @BoxId, '0', '0', '2429', '0', '1', '0', '7'), /* Erenia's Egg */
	(@BoxDesign, @BoxId, '0', '0', '9379', '0', '1', '0', '1'), /* Little Devil */
	(@BoxDesign, @BoxId, '0', '0', '2814', '0', '5', '0', '70'), /* Silk */
	(@BoxDesign, @BoxId, '0', '0', '2812', '0', '3', '0', '70'), /* Intact Orichalcum */
	(@BoxDesign, @BoxId, '0', '0', '2805', '0', '3', '0', '45'), /* Crystal of Balance */
	(@BoxDesign, @BoxId, '0', '0', '2285', '0', '2', '0', '65'), /* Shinig Blue Soul */
	(@BoxDesign, @BoxId, '0', '0', '2513', '0', '1', '0', '60'), /* Dragon Heart */
	(@BoxDesign, @BoxId, '0', '0', '2282', '0', '10', '0', '90'), /* Angel's Feather */
	(@BoxDesign, @BoxId, '0', '0', '5884', '0', '1', '0', '40'), /* Ancelloan's Accessory Production Scroll */
	(@BoxDesign, @BoxId, '0', '0', '5885', '0', '1', '0', '40'), /* Ancelloan's Weapon Production Scroll */
	(@BoxDesign, @BoxId, '0', '0', '5886', '0', '1', '0', '40'), /* Ancelloan's Secondary Weapon Production Scroll */
	(@BoxDesign, @BoxId, '0', '0', '5887', '0', '1', '0', '40'), /* Ancelloan's Armour Production Scroll */
	(@BoxDesign, @BoxId, '0', '0', '1025', '0', '2', '0', '80'), /* Cellon Lv.9 */
	(@BoxDesign, @BoxId, '0', '0', '1025', '0', '2', '0', '80') /* Silk */

