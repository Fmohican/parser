/* Info :
 Jack O'Lantern Level: 20 - 99 
 Player :  5 - 15 :
 Delay : 60 min. 
 Reputation : +100 
 Gold : 1000 - 2000
 Data : 
 Damage% : yes
 TeleportAll : yes
 OneSeal : no
 */
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 10


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES

	(@BoxDesign, @BoxId, '0', '7', '950', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4173', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4174', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4175', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4169', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4170', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4171', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '4172', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '2281', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '1918', '0', '1', '0', '2'),
	(@BoxDesign, @BoxId, '0', '7', '9348', '0', '1', '0', '1')