/* Info Zenas: 
 PeteOPeng Level: 88 - 99 
 Player :  5 - 15 :
*/
DECLARE @BoxId SMALLINT = 302
DECLARE @BoxDesign SMALLINT = 23


INSERT INTO [dbo].[RollGeneratedItem] 
(
	[OriginalItemDesign], [OriginalItemVNum],
	[MinimumOriginalItemRare], [MaximumOriginalItemRare], [ItemGeneratedVNum],
	[ItemGeneratedDesign],	[ItemGeneratedAmount], [IsRareRandom],
	[Probability]
)
VALUES
 
	(@BoxDesign, @BoxId, '0', '8', '4949', '0', '1', '0', '10'), /* Heavenly Heavy Armour */
	(@BoxDesign, @BoxId, '0', '8', '4950', '0', '1', '0', '10'), /* Heavenly Robe */
	(@BoxDesign, @BoxId, '0', '8', '4951', '0', '1', '0', '10'), /* Heavenly Leather Armour */
	(@BoxDesign, @BoxId, '0', '8', '4958', '0', '1', '0', '5'), /* Heavenly Sword */
	(@BoxDesign, @BoxId, '0', '8', '4959', '0', '1', '0', '5'), /* Heavenly Wand */
	(@BoxDesign, @BoxId, '0', '8', '4960', '0', '1', '0', '5'), /* Heavenly Bow */
	(@BoxDesign, @BoxId, '0', '8', '4955', '0', '1', '0', '10'), /* Heavenly Crossbow */
	(@BoxDesign, @BoxId, '0', '8', '4956', '0', '1', '0', '10'), /* Heavenly Spellgun */
	(@BoxDesign, @BoxId, '0', '8', '4957', '0', '1', '0', '10'), /* Heavenly Dagger */
	(@BoxDesign, @BoxId, '0', '0', '4971', '0', '1', '0', '20'), /* Heavenly Leather Hat */
	(@BoxDesign, @BoxId, '0', '0', '4941', '0', '1', '0', '20'), /* Heavenly Helmet */
	(@BoxDesign, @BoxId, '0', '0', '4973', '0', '1', '0', '20'), /* Heavenly  Headband*/
	(@BoxDesign, @BoxId, '0', '0', '4968', '0', '1', '0', '5'), /* Heavenly Shoes */
	(@BoxDesign, @BoxId, '0', '0', '4967', '0', '1', '0', '5'), /* Heavenly Gloves */
	(@BoxDesign, @BoxId, '0', '0', '4946', '0', '1', '0', '5'), /* Heavenly Neckle */
	(@BoxDesign, @BoxId, '0', '0', '4947', '0', '1', '0', '5'), /* Heavenly Ring */
	(@BoxDesign, @BoxId, '0', '0', '4948', '0', '1', '0', '5'), /* Heavenly Bracel */
	(@BoxDesign, @BoxId, '0', '0', '4976', '0', '1', '0', '1'), /* Zenas Shoes */
	(@BoxDesign, @BoxId, '0', '0', '1685', '0', '1', '0', '10'), /* Angel Wings */
	(@BoxDesign, @BoxId, '0', '0', '5431', '0', '1', '0', '2'), /* ArchAngel Wings */
	(@BoxDesign, @BoxId, '0', '0', '2517', '0', '1', '0', '35'), /* Small topaz */
	(@BoxDesign, @BoxId, '0', '0', '2521', '0', '1', '0', '30'), /* topaz */
	(@BoxDesign, @BoxId, '0', '0', '2427', '0', '1', '0', '7'), /* Zenas Egg */
	(@BoxDesign, @BoxId, '0', '0', '9378', '0', '1', '0', '1'), /* Little Angel  */
	(@BoxDesign, @BoxId, '0', '0', '2814', '0', '5', '0', '70'), /* Silk */
	(@BoxDesign, @BoxId, '0', '0', '2812', '0', '3', '0', '70'), /* Intact Orichalcum */
	(@BoxDesign, @BoxId, '0', '0', '2805', '0', '3', '0', '45'), /* Crystal of Balance */
	(@BoxDesign, @BoxId, '0', '0', '2285', '0', '2', '0', '65'), /* Shinig Blue Soul */
	(@BoxDesign, @BoxId, '0', '0', '2513', '0', '1', '0', '60'), /* Dragon Heart */
	(@BoxDesign, @BoxId, '0', '0', '2282', '0', '10', '0', '90'), /* Angel's Feather */
	(@BoxDesign, @BoxId, '0', '0', '5884', '0', '1', '0', '40'), /* Ancelloan's Accessory Production Scroll */
	(@BoxDesign, @BoxId, '0', '0', '5885', '0', '1', '0', '40'), /* Ancelloan's Weapon Production Scroll */
	(@BoxDesign, @BoxId, '0', '0', '5886', '0', '1', '0', '40'), /* Ancelloan's Secondary Weapon Production Scroll */
	(@BoxDesign, @BoxId, '0', '0', '5887', '0', '1', '0', '40'), /* Ancelloan's Armour Production Scroll */
	(@BoxDesign, @BoxId, '0', '0', '1025', '0', '2', '0', '80') /* Cellon Lv.9 */

